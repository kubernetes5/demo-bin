package com.htc.microservices.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeServiceV5Application {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeServiceV5Application.class, args);
	}

}
